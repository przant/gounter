package arrgs

import (
	"flag"
	"fmt"
	"os"
	"runtime"
)

const (
	mirrorHelp = "Representa la `ruta` absoluta o relativa a la carpeta de trabajo.\n"

	suffixHelp = "Es la extensión de los archivos resumen del conteo de palabras.\n"

	summaryHelp = "Es el nombre del archivo con el resumen total de palabras.\n"

	coresHelp = "Cantidad de `procesadores` a usar en al ejecución del programa.\n"

	hostTaskHelp = "Número de tareas simúltaneas a utilizar para procesar carpetas host.\n"

	pageTaskHelp = "Número de tareas simúltaneas por host a utilizar para procesar archivos.\n"
)

var (
	// Mirror es la ruta a la carpeta que almacena los host descargados.
	Mirror string
	// Suffix es la extension de los archivos resumen del conteo de palabras.
	Suffix string
	// Summary archivo que almacena el resumen general del conteo de palabras.
	Summary string
)

var (
	// Cores almacena el numero de procesadores a utilizar en la ejecución
	Cores int
	// HostTask almacena la cantidad de tareas simúltaneas para procesamiento
	// de las carpetas host.
	HostTask int
	// PageTask almacena la cantidad de tareas simúltaneas qye procesarán los
	// archivos que hay por cada carpeta host.
	PageTask int
)

func init() {
	flag.Usage = func() {
		fmt.Printf("uso: %s -dir [-suf file_suffix] [-out filename] [-ht host_task] [-pt page_task] [-cpus cores]\n", os.Args[0])
		flag.PrintDefaults()
		fmt.Println()
	}

	setFlags()
	checkFlags()
	runtime.GOMAXPROCS(Cores)
}

func setFlags() {
	hTask := 5
	pTask := 5
	wd := os.Getenv("PWD")
	suffx := "_summary"
	outfile := "total_words"
	flag.IntVar(&Cores, "cpus", 1, coresHelp)
	flag.IntVar(&HostTask, "ht", hTask, hostTaskHelp)
	flag.IntVar(&PageTask, "pt", pTask, pageTaskHelp)
	flag.StringVar(&Mirror, "dir", wd, mirrorHelp)
	flag.StringVar(&Suffix, "suf", suffx, suffixHelp)
	flag.StringVar(&Summary, "out", outfile, summaryHelp)
	flag.Parse()
}

func checkFlags() {

	if Cores <= 1 || Cores >= runtime.NumCPU() {
		Cores = 1
	}

	switch {
	case flag.Parsed() && flag.NFlag() < 1:
		flag.Usage()
		os.Exit(2)
	case flag.Parsed() && flag.NFlag() == 1:
		if _, err := os.Open(Mirror); err != nil {
			switch {
			case err == os.ErrNotExist:
				fmt.Printf("\nError no existe la carpeta %q: %s\n", Mirror, err)
				flag.Usage()
				os.Exit(2)
			case err == os.ErrInvalid:
				fmt.Printf("\nError ruta inválida %q: %s\n", Mirror, err)
				flag.Usage()
				os.Exit(2)
			}
		}
	}
}
